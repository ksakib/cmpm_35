  function init(elements) {


  let canvasW = 700;
  let canvasH = 400;

  let margin = 50;
  let w = canvasW - (margin * 2);
  let h = canvasH - (margin * 2);
  let xinc = w / 10;
  let yinc = h / 10;

  let svg = d3.select("body").append("svg")
    .attr("width", canvasW)
    .attr("height", canvasH)
    ;
    let imgs = svg.selectAll("image").data([0]);
                  imgs.enter()
                  .append("svg:image")
                  .attr("xlink:href", "night_sky.jpg")
                  .attr("x", "0")
                  .attr("y", "0")
                  .attr("width", "700")
                  .attr("height", "400");

  let jsonAMs;
  let jsonPMs;
  let jsonsleeps;


  let jsonAMs_Sunday = elements.filter( e => {  return e.AMPM == "am" && e.Day == "Sunday";  } );
  let jsonPMs_Sunday = elements.filter( e => {  return e.AMPM == "pm" && e.Day == "Sunday";  } );
  let jsonsleeps_Sunday = elements.filter( e => {  return e.Day == "Sunday";  } );

  // let jsonAMs_Monday = elements.filter( e => {  return e.AMPM == "AM" && e.Day == "Monday";  } );
  // let jsonPMs_Monday = elements.filter( e => {  return e.AMPM == "PM" && e.Day == "Monday";  } );
  // let jsonsleeps_Monday = elements.filter( e => {  return e.Day == "Monday";  } );

  jsonAMs = jsonAMs_Sunday;
  jsonPMs = jsonPMs_Sunday;
  jsonsleeps = jsonsleeps_Sunday;

  /*
  jsonAMs = jsonAMs_Monday;
  jsonPMs = jsonPMs_Monday;
  jsonsleeps = jsonsleeps_Monday;
  */



  let circles = svg.selectAll()
    .data(jsonPMs)
    .enter() //when we are seeing new AM data for the first time
      .append("circle") //append a circle shape for each data point, and set various attributes based on the data
        .attr("fill", d3.color("rgba(215,215,215,0.5)")  )
        .attr("cx", d => { return margin + (d.Efficiency * xinc); })
        .attr("cy", d => { return canvasH - (margin + (d.Hours * yinc)); })
        .attr("r", 30)
        .attr("stroke", d3.color("rgba(0,0,0,0.5)") )
        .attr("stroke-width", 3)
    ;


  let rects = svg.selectAll()
    .data(jsonAMs)
    .enter() //when we are seeing new PM data for the first time
      .append("rect") //append a rect shape for each data point, and set various attributes based on the data
        .attr("fill", d3.color("rgba(255,170,30,0.5)")  )
        .attr("x", d => { return -30 + margin + (d.Efficiency * xinc); })
        .attr("y", d => { return -30 + canvasH - (margin + (d.Hours * yinc)); })
        .attr("width", d => { return 60; })
        .attr("height", d => { return 60; })
        .attr("stroke", d3.color("rgba(0,0,0,0.5)") )
        .attr("stroke-width", 3)
    ;




  let text = svg.selectAll()
    .data(jsonsleeps)
    .enter();


  text
    .append("text")
    .attr("text-anchor","middle")
    .attr("font-family", "sans-serif")
    .attr("font-size", "16px")
    .attr("fill", "white")
    .attr("x", d => { return -30 + margin + (d.Efficiency * xinc) + 30; })
    .attr("y", d => { return -30 + canvasH - (margin + (d.Hours * yinc)) + 80; })
    .text(d => {return d.Rating});

  text
    .append("text")
    .attr("text-anchor","middle")
    .attr("font-family", "sans-serif")
    .attr("font-size", "16px")
    .attr("fill", "white")
    .attr("x", d => { return -30 + margin + (d.Efficiency * xinc) + 30; })
    .attr("y", d => { return -30 + canvasH - (margin + (d.Hours * yinc)) + 35; })
    .text(d => {return d.Efficiency})





  const xText = svg.append("text")
    .attr("x",  canvasW / 2)
    .attr("y", canvasH - 10)
    .attr("text-anchor","middle")
    .attr("font-family", "sans-serif")
    .attr("font-size", "24px")
    .attr("fill", "white")
    .text("Efficiency of sleep");

  const yText = svg.append("text")
    .attr("text-anchor","middle")
    .attr("font-family", "sans-serif")
    .attr("font-size", "24px")
    .attr("fill", "white")
    .attr("transform", "translate(10,"+(canvasH/2)+") rotate(90)")
    .text("Hours of sleep");




}
