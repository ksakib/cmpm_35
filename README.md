# D3.js + SVG assignment
Both part 1 & part 2 included with visual screenshot in respective folders.

## SVG assignment
This part of assignment is in the SVG_assignment folder. 
### Assignment description
This is the part 1 of the assignment. I tried my best to make a drawing with svg shapes of Deadpool. Used svg text, circle, path and more. Used third-party website to produce random shapes: 

```
Title: Blobmaker
Publisher: z creative labs
Date: 2020
Version: 2.0
Link: https://www.blobmaker.app/ 
```
### File contents
* **deadpool.html** - the main html file
* **shape.svg** - the svg file containing all the shapes
* **svg_screenshot.png** - the visual screenshot


## D3.js assignment
This part of assignment is in the d3_load_assignment folder. 
### Assignment description
This is the part 2 of the assignment. It's a modified load.js from the example with my part of my own dataset used. This chart shows my hours of sleep and the efficiency of it tracked by my galaxy active 2 smartwatch. To fit in the load.js program I only used few sunday result from my extensive dataset.
### File contents
* **d3_screenshot.png** - the visual screenshot
* **load.html** - the main html file
* **load.js** - the javascript file containing d3 example
* **night_sky.jpg** - the image used for background
* **sleepschedule.csv** - the modified dataset from my own dataset



